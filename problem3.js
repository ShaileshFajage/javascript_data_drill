

let userDetailsArray = require(`./userDetailsArray`)

function getDetailsUsingReduce(arr) {

    const result = arr.reduce(function (resArr, b){

        let key = b.id;
        resArr[key] = b;

        return resArr;

        
    },{})

    return result;
}

console.log(getDetailsUsingReduce(userDetailsArray));