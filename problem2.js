
let userDetailsArray = require(`./userDetailsArray`)

function getAddressUsingReduce(arr) {

    const result = arr.reduce(function (resArr, b){

        let key = b.id;
        resArr[key] = b.address;

        return resArr;

        
    },{})

    return result;
}

console.log(getAddressUsingReduce(userDetailsArray));