

let userDetailsArray = require(`./userDetailsArray`)


function getDetails(arr) {

    const result = arr.reduce(function (resArr, b){

        if(b.address.city==='North Dakota')
        {
            resArr[b.id] = {information :  [b.name, b.address.city, b.company]}
        }

        return resArr;
        
    },{})

    return result;
}

console.log(getDetails(userDetailsArray));