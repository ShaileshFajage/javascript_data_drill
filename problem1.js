
let userDetailsArray = require(`./userDetailsArray`)


function  getAddressUsingMap(arr) {


    result = arr.map(function (value) {
        return value.address;
    })

    return result;
    
}

function getAddressUsingReduce(arr) {
    let i=0;

    const result = arr.reduce(function (resArr, b){

        resArr.push(b.address);
        return resArr;
    },[])

    return result;
}

console.log(getAddressUsingMap(userDetailsArray));

console.log(getAddressUsingReduce(userDetailsArray));